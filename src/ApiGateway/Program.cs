using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.RateLimiting;
using ApiGateway.Endpoints;
using ApiGateway;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddReverseProxy()
            .LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"));

var tokenOptions = builder.Configuration.GetSection("TokenOptions");
builder.Services.Configure<TokenOptions>(tokenOptions);

var tokenOptionsKey = tokenOptions.Get<TokenOptions>();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(cfg =>
    {

        cfg.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidIssuer = tokenOptionsKey.Issuer,
            ValidateAudience = true,
            ValidAudience = tokenOptionsKey.Audience,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenOptionsKey.SecurityKey)),
        };
    });

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("authenticated", policy =>
        policy.RequireAuthenticatedUser());
});

builder.Services.AddRateLimiter(rateLimiterOptions =>
{
    rateLimiterOptions.AddFixedWindowLimiter("fixed", options =>
    {
        options.Window = TimeSpan.FromSeconds(10);
        options.PermitLimit = 5;
    });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseAuthentication();

app.UseRateLimiter();

app.MapControllers();

app.UseRouting();

app.UseAuthorization();
app.MapReverseProxy();

app.MapPost("/tokens/connect", ([FromBody] UserLoginRequest user)
    => TokenEndpoint.Connect(user));

//read the jwt token from header
app.MapGet("/jwt-token/headers", (HttpContext ctx) =>
{
    if (ctx.Request.Headers.TryGetValue("Authorization", out var headerAuth))
    {
        var jwtToken = headerAuth.First().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
        return Task.FromResult(TypedResults.Ok(new { token = jwtToken }));
    }
    return Task.FromResult(TypedResults.NotFound(new { message = "jwt not found" }));
});

//read the jwt token from authentication context
app.MapGet("/jwt-token/context", async (HttpContext ctx) =>
{
    var token = await ctx.GetTokenAsync("access_token");

    return TypedResults.Ok(new { token = token });
});

app.Run();
