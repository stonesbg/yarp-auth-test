using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
namespace ApiGateway.Endpoints;

public static class TokenEndpoint
{
	public static async Task<IResult> Connect(UserLoginRequest user)
	{
		if (user is null)
		{
			return Results.BadRequest("Invalid user request!!!");
		}
		if (user.Username == "admin" && user.Password == "admin")
		{
			var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("this is my custom Secret key for authentication"));  // TODO replace with Configuration section
			var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
			var tokeOptions = new JwtSecurityToken(
							issuer: "some@mailaddress.com", // TODO replace with Configuration section
							audience: "some@mailaddress.com", // TODO replace with Configuration section
							claims: new List<Claim>(),
							expires: DateTime.Now.AddMinutes(10), // TODO replace with Configuration section
							signingCredentials: signinCredentials
			);
			var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
			return Results.Ok(new JWTTokenResponse { Token = tokenString });
		}
		return Results.Unauthorized();
	}
}